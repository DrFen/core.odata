# Core.OData

Подключение методов OData и регистрации контроллеров

## Установка
Установка либо из репозитория или Nuget пакета
*   Репозиторий модуля [http://afina.itps.com/dsc/hse2/Core.OData.git](http://afina.itps.com/dsc/hse2/Core.OData.git)
*   NuGet пакет **itps.Core.OData**

## Настройка

Вызвать метод регистрации динамических контроллеров в Startup.cs **ConfigureServices(IServiceCollection services)** 
```C#
	services.AddMvc()
	 .SetCompatibilityVersion(CompatibilityVersion.Version_2_0)
	 .RegisterDynamicControllers();

	services.AddOData();
```

В методе **Configure(IApplicationBuilder app, IHostingEnvironment env)** необходимо установить парметры маршрутизации вызвав метод ItpsODataRegistration.RegisterRouteBuilder
```C#
	  app.UseMvc(routeBuilder =>
		{
			routeBuilder.MapRoute(name: "default",
								  template: "{action}/{id?}");

			ItpsODataRegistration.RegisterRouteBuilder(routeBuilder);
		});
```

## Подключение моделей
### Создание модели

Модель должна быть отнаследована от абстрактного класса  **ModelEntity<TKey>** где **TKey** тип данных ключа сущности
Если необходима автоматическая регистрация контроллера для класса должен быть установлен **ODataModelAttribute**, например:
```C#
	[ODataModel]
    public class ODataDummyModel : ModelEntity<int>
    {
        public string StringValue { get; set; }
      
        public decimal DoubleValue { get; set; }
        public string[] StringArrayValue { get; set; }
        public DummyEntity EntityValue { get; set; }
        
    }
```

### Автоматическое создание контроллеров
Необходимо реализовать сервис реализующий интерфейс **IServiceBase<TModel, TKey>** где **TModel** - тип модели, **TKey** - тип ключа модели
```C#
	/// <summary>
    /// Сервис получения данных
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public interface IServiceBase<TModel, TKey> 
        where TModel : ModelEntity<TKey>
        //where TKey : struct
    {
        /// <summary>
        /// Получение сущности по ключу
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TModel> GetById(TKey id);

        /// <summary>
        /// Получение списка сущностей
        /// </summary>
        /// <returns></returns>
        Task<IQueryable<TModel>> GetCollection();

        /// <summary>
        /// Добавление сущности
        /// </summary>
        /// <param name="model">Модель</param>
        /// <returns>Идентификатор модели</returns>
        Task<TKey> Insert(TModel model);

        /// <summary>
        /// Обновление сущности
        /// </summary>
        /// <param name="model">Новые значения модели</param>
        /// <returns>Обновленная модель/returns>
        Task Update(TModel model);

        /// <summary>
        /// Удаление сущности
        /// </summary>
        /// <param name="id">Идентификатор</param>
        Task Delete(TKey id);
    }
```

И зарегистрировать его в DI