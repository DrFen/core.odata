﻿using System.Linq;
using System.Threading.Tasks;
using Core.OData.Domain;
using Microsoft.AspNet.OData;

namespace Core.OData.Services
{
    /// <summary>
    /// Сервис получения данных
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public interface IServiceBase<TModel, TKey> 
        where TModel : ModelEntity<TKey>
        //where TKey : struct
    {
        /// <summary>
        /// Получение сущности по ключу
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TModel> GetById(TKey id);

        /// <summary>
        /// Получение списка сущностей
        /// </summary>
        /// <returns></returns>
        IQueryable<TModel> GetCollection();

        /// <summary>
        /// Добавление сущности
        /// </summary>
        /// <param name="model">Модель</param>
        /// <returns>Идентификатор модели</returns>
        Task<TKey> Insert(TModel model);

        /// <summary>
        /// Обновление сущности
        /// </summary>
        /// <param name="id">Ключ определённый для модели</param>
        /// <param name="delta">Измененные значения модели</param>
        /// <returns>Обновленная модель</returns>
        Task Update(TKey id, Delta<TModel> delta);

        /// <summary>
        /// Удаление сущности
        /// </summary>
        /// <param name="id">Идентификатор</param>
        Task Delete(TKey id);
    }
}
