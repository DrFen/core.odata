﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Core.OData.Services
{
    public interface IRequestInvoker
    {
        /// <summary>
        /// Выполнение действия получения списка
        /// </summary>
        /// <param name="action">Делегат метода сервиса</param>
        /// <returns></returns>
        Task<IActionResult> Get<TResult>(Func<Task<TResult>> action);

        /// <summary>
        /// Выполнение действия вставки
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        Task<IActionResult> Post<TResult>(Func<Task<TResult>> action);

        /// <summary>
        /// Выполнение действия обновления
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        Task<IActionResult> Patch<TResult>(Func<Task<TResult>> action);

        /// <summary>
        /// Выполнение действия удаления
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        Task<IActionResult> Delete<TResult>(Func<Task<TResult>> action);
    }
}
