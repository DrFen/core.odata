﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Core.OData.Services;
using Microsoft.AspNet.OData.Results;

namespace Core.OData.Domain
{

    public class RequestInvoker : IRequestInvoker
    {

        #region fields
        /// <summary>
        /// Лениво-инициализирующийся инстанс 
        /// </summary>
        private static readonly Lazy<RequestInvoker> Lazy = new Lazy<RequestInvoker>(() => new RequestInvoker());

        /// <summary>
        /// Инстанс синглтона
        /// </summary>
        public static RequestInvoker Instance => Lazy.Value;


        /// <summary>
        /// Выполнитель запросов
        /// </summary>
        private IRequestInvoker _invoker;


        #endregion fields


        #region constructor
        private RequestInvoker() { }
        #endregion constructor

        #region methods

        /// <summary>
        /// Установка исполнителя запросов
        /// </summary>
        /// <param name="invoker"></param>
        public void SetInvoker(IRequestInvoker invoker)
        {
            _invoker = invoker;
        }
        #endregion methods


        public async Task<IActionResult> Get<TResult>(Func<Task<TResult>> action)
        {
            if (_invoker != null)
                return await _invoker.Get(action);

            return new OkObjectResult(await action());
        }

        public async Task<IActionResult> Post<TResult>(Func<Task<TResult>> action)
        {
            if (_invoker != null)
                return await _invoker.Post(action);

            return new OkObjectResult(await action());
        }

        public async Task<IActionResult> Patch<TResult>(Func<Task<TResult>> action)
        {
            if (_invoker != null)
                return await _invoker.Patch(action);

            return new UpdatedODataResult<TResult>(await action());
        }

        public async Task<IActionResult> Delete<TResult>(Func<Task<TResult>> action)
        {
            if (_invoker != null)
                return await _invoker.Delete(action);

            await action();
            return new StatusCodeResult((int)HttpStatusCode.NoContent);

        }

    }
}
