﻿using System.ComponentModel.DataAnnotations;

namespace Core.OData.Domain
{
    /// <summary>
    /// Модель сущности
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public abstract class ModelEntity<TKey> //where TKey : struct
    {
        [Key]
        public virtual TKey Id { get; set; }

    }
}
