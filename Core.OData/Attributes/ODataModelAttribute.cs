﻿using System;

namespace Core.OData.Attributes
{
    public class ODataModelAttribute : Attribute
    {
        public bool IsComplexType { get; set; }
    }
}
