﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System;

namespace Core.OData.Attributes
{
    /// <summary>
    /// Атрибут присвоения имя генерик контролеру
    /// </summary>
    public class GenericControllerAttribute : Attribute, IControllerModelConvention
    {
        /// <summary>
        /// Применение при инициалиции контроллера
        /// </summary>
        /// <param name="controller"></param>
        public void Apply(ControllerModel controller)
        {
            Type entityType = controller.ControllerType.GetGenericArguments()[0];
            controller.ControllerName = entityType.Name;

        }
    }
}
