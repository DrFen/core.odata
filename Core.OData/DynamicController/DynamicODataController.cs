﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Core.OData.Attributes;
using Core.OData.Domain;
using Core.OData.Services;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Mvc;

namespace Core.OData.DynamicController
{
    [Produces("application/json")]
    [GenericController]
    public class DynamicODataController<TModel, TKey> : ODataController
        where TModel : ModelEntity<TKey>
        //where TKey : struct
    {
        #region fields
        /// <summary>
        /// Основной сервис работы с сущностью
        /// </summary>
        private IServiceBase<TModel, TKey> _service;
        #endregion fields

        #region constructor
        /// <summary>
        /// Основной контроллер
        /// </summary>        

        public DynamicODataController(IServiceBase<TModel, TKey> service)
        {
            _service = service;
        }


        #endregion constructor

        #region methods
        /// <summary>
        /// Получение списка
        /// </summary>
        /// <param name="odataQueryOptions">Опции запроса</param>
        /// <returns></returns>
        [EnableQuery]                
        public IQueryable<TModel> Get(ODataQueryOptions<TModel> odataQueryOptions)
        {
            return _service.GetCollection();

        }

        /// <summary>
        /// Получение сущности
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>           
        [EnableQuery]
        public async Task<IActionResult> Get([FromODataUri] TKey key)
        {
            return await RequestInvoker.Instance.Get(async () => await _service.GetById(key));
        }

        /// <summary>
        /// </summary>
        /// <param name="model">Модель</param>
        /// <returns>Идентификатор сущности</returns>
        public async Task<IActionResult> Post([FromBody]TModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return await RequestInvoker.Instance.Post(async () => await _service.Insert(model));

        }

        /// <summary>
        /// Обновление сущности
        /// </summary>
        /// <param name="key"></param>
        /// <param name="delta"></param>
        /// <returns></returns>
        public async Task<IActionResult> Patch([FromODataUri] TKey key, Delta<TModel> delta)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            return await RequestInvoker.Instance.Post(async () =>
            {
                await _service.Update(key, delta);
                return delta;
            });

        }

        /// <summary>
        /// Удаление
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(TKey key)
        {
            return await RequestInvoker.Instance.Delete(async () =>
            {
                await _service.Delete(key);
                return true;
            });

            
        }
        #endregion methods



    }
}