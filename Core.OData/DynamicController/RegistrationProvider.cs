﻿using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Core.OData.DynamicController
{
    public class RegistrationProvider : IApplicationFeatureProvider<ControllerFeature>
    {
        #region methods


        /// <summary>
        /// Провайдер регистрации
        /// </summary>
        /// <param name="parts"></param>
        /// <param name="feature"></param>
        public void PopulateFeature(IEnumerable<ApplicationPart> parts, ControllerFeature feature)
        {

            ItpsODataRegistration.GetModelTypes()
                .ForEach(f =>
                {
                    var idProperty = f.Type.GetProperty("Id");                   


                    feature.Controllers.Add(typeof(DynamicODataController<,>).MakeGenericType(new Type[] { f.Type.GetTypeInfo(), idProperty.PropertyType }).GetTypeInfo());

                });

        }
        #endregion methods

    }
}
