﻿using Core.OData.Attributes;
using Core.OData.DynamicController;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OData.Edm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.OData.Domain;
using Core.OData.Services;

namespace Core.OData
{
    /// <summary>
    /// Вспомогательный класс регистрации сервисов для подключения OData
    /// </summary>
    public static class ItpsODataRegistration
    {
        private static IEdmModel GetEdmModel()
        {
            var builder = new ODataConventionModelBuilder();

            //  builder.EntitySet<ODataDummyModel>("ODataDummyModel");
            GetModelTypes()
                .ForEach(f =>
                {
                    if (f.IsComplex)
                    {
                        builder.AddComplexType(f.Type.GetTypeInfo());
                    }
                    else
                    {
                        builder.AddEntitySet(f.Type.GetTypeInfo().Name, builder.AddEntityType(f.Type.GetTypeInfo()));
                    }

                });
            return builder.GetEdmModel();
        }

        /// <summary>
        /// Регистрация route OData
        /// </summary>
        /// <param name="routeBuilder"></param>
        public static void RegisterRouteBuilder(IRouteBuilder routeBuilder)
        {
            routeBuilder.Count().Filter().OrderBy().Expand().Select().MaxTop(null);
            routeBuilder.MapODataServiceRoute("odata", "odata", GetEdmModel());
            routeBuilder.EnableDependencyInjection();
        }

        /// <summary>
        /// Подключение динамических OData контроллеров
        /// </summary>
        /// <param name="mvcBuilder"></param>
        public static void RegisterDynamicControllers(this IMvcBuilder mvcBuilder)
        {
            mvcBuilder.ConfigureApplicationPartManager(m => m.FeatureProviders.Add(new RegistrationProvider()));
        }

        /// <summary>
        /// Регистрация обёртки запросов
        /// </summary>
        /// <param name="invoker"></param>
        public static void RegisterInvoker(IRequestInvoker invoker)
        {
            RequestInvoker.Instance.SetInvoker(invoker);
        }

        /// <summary>
        /// Получение моделей OData
        /// </summary>
        /// <returns></returns>
        public static List<ModelType> GetModelTypes()
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(w => w.GetCustomAttributes(typeof(ODataModelAttribute), true).Any())
                .Select(s=> new ModelType
                {
                     Type = s,
                     IsComplex = s.GetCustomAttribute<ODataModelAttribute>(true).IsComplexType
                })
                .ToList();
        }
    }

    public class ModelType
    {
        public Type Type { get; set; }
        public bool IsComplex { get; set; }
    }
}
