﻿using Core.OData.Attributes;
using Core.OData.Domain;

namespace ODataTestWeb.Model
{
    [ODataModel]
    public class DummyModel : ModelEntity<int>
    {
        public string Name { get; set; }
    }
}
