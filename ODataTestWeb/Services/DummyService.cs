﻿using System.Linq;
using System.Threading.Tasks;
using Core.OData.Services;
using Microsoft.AspNet.OData;
using ODataTestWeb.Model;

namespace ODataTestWeb.Services
{
    public class DummyService : IServiceBase<DummyModel, int>
    {
        public Task Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<DummyModel> GetById(int id)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<DummyModel> GetCollection()
        {

            return new[]
            {
                new DummyModel {Id = 1, Name = "1"},
                new DummyModel {Id = 2, Name = "2"}
            }.AsQueryable();

        }

        public Task<int> Insert(DummyModel model)
        {
            throw new System.NotImplementedException();
        }

        public Task Update(int id, Delta<DummyModel> delta)
        {
            throw new System.NotImplementedException();
        }
    }
}
